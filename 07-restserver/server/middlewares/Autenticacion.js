const jwt = require('jsonwebtoken');

// ===== VERIFICAR TOKEN =====
let verificaToken = (req, res, next) => {

    let token = req.get('Authorization');
    jwt.verify(token, process.env.SEED, (err, decoded) => {

        if(err){
            return res.status(401).json({
                ok: false,
                err
            })
        }

        req.usuario = decoded.usuario;
        next();

    })

}

let verificaAdminRole = (req, res, next) => {
    let user = req.usuario;
        if(user.role === 'ADMIN_ROLE'){
            next();
        }else{
            return res.status(401).json({
                ok: false,
                err: {
                    message: "Solo administradores pueden crear y actualizar usuarios"
                }
            })
        }

}

module.exports = {
    verificaToken,
    verificaAdminRole
}