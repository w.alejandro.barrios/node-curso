const express = require('express');
const Usuario = require('../models/usuario');
const app = express();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

app.post('/auth', (req, res) => {

    let body = req.body;

    Usuario.findOne({ email: body.email }, (err, usrDb) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(!usrDb){
            return res.status(400).json({
                ok: false,
                err: {
                    message: "(Usuario) o contraseña incorrectos"
                }
            })
        }

        if(!bcrypt.compareSync(body.password, usrDb.password)){
            return res.status(400).json({
                ok: false,
                err: {
                    message: "Usuario o (contrasena) incorrectos"
                }
            })
        }

        let token = jwt.sign({
            usuario: usrDb
        }, process.env.SEED, {expiresIn: process.env.CADUCIDAD_TOKEN });

        res.json({
            ok: true,
            usuario: usrDb,
            token
        })

    });
});

module.exports = app;