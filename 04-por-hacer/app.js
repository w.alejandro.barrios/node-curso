const argv = require('./config/yargs').argv;
const colors = require('colors');

const porHacer = require('./por-hacer/por-hacer');

let comando = argv._[0];

switch (comando) {
    case 'crear':
        let tarea = porHacer.crear(argv.descripcion);;
        break;

    case 'listar':
        let listado = porHacer.getListado();
        if (listado.length > 0) {
            for (let tarea of listado) {
                console.log('=========================='.green);
                console.log('Tarea: ', tarea.descripcion);
                console.log('Estado: ', tarea.completado);
                console.log('=========================='.green);
            }
        } else {
            console.log('=============================='.green);
            console.log('NO TIENES TAREAS POR HACER');
            console.log('=============================='.green);
        }

        break;

    case 'actualizar':
        let actualizado = porHacer.actualizar(argv.descripcion, argv.completado);
        console.log(actualizado);
        break;

    case 'borrar':
        let borrar = porHacer.borrar(argv.descripcion);
        console.log(borrar);
        break;

    default:
        console.log('comando invalido');
        break;

}