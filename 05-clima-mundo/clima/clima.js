const axios = require('axios');

const getClima = async (lat, lng) => {

    const resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&appid=865cb36c6bbedc8c1ef5b6e28aa5947f&units=metric`);

    if(!resp.data.main.temp){
        throw new Error('No se pudo determinar el clima');
    }
    return resp.data.main.temp;

}

module.exports = {
    getClima
}