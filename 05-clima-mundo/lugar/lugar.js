const axios = require('axios');

const getLugarLatLng = async (direccion) => {

    let encodedUrl = encodeURI(direccion);

    const instance = axios.create({
        baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${encodedUrl}`,
        headers: { 'X-RapidAPI-Key': '67ced4ed90msh661935658e52863p17b8bcjsnd6bc1bbc0b64' }
    });

    const resp = await instance.get();

    if (resp.data.Results.length === 0) {
        throw new Error(`No se pudo determinar el clima de ${direccion}`);
    }

    const data = resp.data.Results[0];
    const direction = data.name;
    const lat = data.lat;
    const lng = data.lon;

    return {
        direction,
        lat,
        lng
    }

}

module.exports = {
    getLugarLatLng
}