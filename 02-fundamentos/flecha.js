// function sumar(a, b){
//     return a + b;
// }
let sumar = (a, b) => a + b;
// Si recibe un solo parametro, se puede dejar sin parentesis
let saludar = () => 'Hola mundo';
//console.log(saludar());

let deadpool = {
    nombre: 'wade',
    apellido: 'wilson',
    poder: 'regeneracion',
    getNombre(){
        return `${this.nombre} ${this.apellido} - poder: ${this.poder}`;
    }
}

console.log(deadpool.getNombre());
