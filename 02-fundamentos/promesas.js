let empleados = [{
    id: 1,
    nombre: 'Fernando'
},{
    id: 2,
    nombre: 'Gaby'
},{
    id: 3,
    nombre: 'Roberto'
}];

let salarios = [{
    id: 1,
    salario: '$1000'
},{
    id: 2,
    salario: '$2000'
}];

let getEmpleado = (id) => {

    return new Promise((resolve, reject)=>{
        let empleadoDB = empleados.find(empleado => empleado.id === id);

        if(!empleadoDB){
            reject(`No existe el empleado con el ID: ${id}`);
        }else{
            resolve(empleadoDB);
            // callback(null, empleadoDB); - No llamar mas de una vez el callback
        }
    });

}

let getSalario = (empleadoSearch) => {
    return new Promise((resolve, reject) =>{
        let empleadoDB;
        let salarioDB;
        if(empleadoSearch){
            empleadoDB = empleados.find(empleado => empleado.id === empleadoSearch.id);
            salarioDB = salarios.find(salario => salario.id === empleadoSearch.id);
        }

        if(!empleadoDB){
            reject(`No existe el empleado`);
        }else if(!salarioDB){
            reject(`No existe un salario para el empleado ${empleadoSearch.nombre}`);
        }else{
            resolve({
                nombre: empleadoSearch.nombre,
                salario: salarioDB.salario
            });
        }
    });

}

getEmpleado(1).then(empleado =>{
    return getSalario(empleado);
})
.then(resp => {
    console.log(`El salario de ${resp.nombre} es de ${resp.salario}`);
})
.catch(err => {
    console.log(err);
});

// getSalario(empleados[1]).then(response =>{
//     console.log(`El empleado ${response.nombre} tiene un salario de ${response.salario}`);
// }, (err) =>{
//     console.log(err);
// });