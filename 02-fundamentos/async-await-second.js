let empleados = [{
    id: 1,
    nombre: 'Fernando'
},{
    id: 2,
    nombre: 'Gaby'
},{
    id: 3,
    nombre: 'Roberto'
}];

let salarios = [{
    id: 1,
    salario: '$1000'
},{
    id: 2,
    salario: '$2000'
}];

let getEmpleado = async (id) => {

        let empleadoDB = empleados.find(empleado => empleado.id === id);

        if(!empleadoDB){
            throw new Error(`No existe el empleado con el ID: ${id}`);
        }else{
            return empleadoDB;
            // callback(null, empleadoDB); - No llamar mas de una vez el callback
        }

}

let getSalario = async (empleadoSearch) => {
        let empleadoDB;
        let salarioDB;
        if(empleadoSearch){
            empleadoDB = empleados.find(empleado => empleado.id === empleadoSearch.id);
            salarioDB = salarios.find(salario => salario.id === empleadoSearch.id);
        }

        if(!empleadoDB){
            throw new Error(`No existe el empleado`);
        }else if(!salarioDB){
            throw new Error(`No existe un salario para el empleado ${empleadoSearch.nombre}`);
        }else{
            return{
                nombre: empleadoSearch.nombre,
                salario: salarioDB.salario
            };
        }

}

let getInformacion = async (id) => {
    
    let empleado = await getEmpleado(id);
    let resp = await getSalario(empleado);
    console.log(empleado);
    console.log(resp);

    return `${resp.nombre} tiene un salario de ${resp.salario}`;

}

getInformacion(4)
    .then(msg => console.log(msg))
    .catch(err => console.log(err.message));