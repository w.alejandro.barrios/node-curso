let empleados = [{
    id: 1,
    nombre: 'Fernando'
},{
    id: 2,
    nombre: 'Gaby'
},{
    id: 3,
    nombre: 'Roberto'
}];

let salarios = [{
    id: 1,
    salario: '$1000'
},{
    id: 2,
    salario: '$2000'
}];

let getEmpleado = (id, callback) => {
    let empleadoDB = empleados.find(empleado => empleado.id === id);

    if(!empleadoDB){
        callback(`No existe el empleado con el ID: ${id}`);
    }else{
        callback(null, empleadoDB);
        // callback(null, empleadoDB); - No llamar mas de una vez el callback
    }
}

let getSalario = (empleadoSearch, callback) => {
    let empleadoDB;
    let salarioDB;
    if(empleadoSearch){
        empleadoDB = empleados.find(empleado => empleado.id === empleadoSearch.id);
        salarioDB = salarios.find(salario => salario.id === empleadoSearch.id);
    }

    if(!empleadoDB){
        callback(`No existe el empleado`);
    }else if(!salarioDB){
        callback(`No existe un salario para el empleado ${empleadoSearch.nombre}`);
    }else{
        callback(null, {
            nombre: empleadoSearch.nombre,
            salario: salarioDB.salario
        });
    }
}

// getEmpleado(1, (err, empleado) => {
//     if(err){
//         return console.log(err);
//     }
//     console.log(empleado);
// });

getSalario(empleados[1], (err, response) => {
    if(err){
        return console.log(err);
    }
    console.log(response);
});