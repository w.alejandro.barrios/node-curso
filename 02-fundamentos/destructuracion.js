let deadpool = {
    nombre: 'wade',
    apellido: 'wilson',
    poder: 'regeneracion',
    getNombre: function(){
        return `${this.nombre} ${this.apellido} - poder: ${this.poder}`;
    }
}

// Usando destructuracion
let { nombre: primerNombre, apellido, poder } = deadpool;
console.log(primerNombre, apellido, poder);